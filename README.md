# README #
---
## Mathe III Formelsammlung ##

[aktuelles PDF](https://bytebucket.org/onkelpit/mathe3/raw/HEAD/Mathe3.pdf)

Eine kleine Formelsammlung für das Modul Mathematik III Inf an der HS Emden-Leer.

### Inhalt: ###
* Kombinatorik :white_check_mark:
	+ Permutation
	+ Variation 
	+ Kombination
* Wahrscheinlichkeit-Rechnung :white_check_mark:
	+ LaPlace
	+ Kolmogorff
	+ bedingte Wahrscheinlichkeit
	+ geometrische Wahrscheinlichkeit 
	+ Zufallsvariable X
	+ Binomialverteilung
	+ Normalverteilung
* Numerik :white_check_mark:
	+ Mittelpuktsregel
	+ Trapezregel
	+ Simpsonregel
	+ Regula Falsi
	+ Newton-Verfahren
	+ Bisektionsverfahren
	+ Fehlerrechnung
* Allgemein :white_check_mark:
	+ Ableitungsregel
	+ ln x und e^x

### Mitglieder: ###
* Peter Johennecken
* Jens Fisser
* Marco Hoffmeier
